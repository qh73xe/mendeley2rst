# MENDELAY2rst #

このスクリプトは [ Mendeley ](https://www.mendeley.com/) のデスクトップアプリで管理をしている DB から、
論文情報を参照し、指定の論文の pdf を [ sphinx/rst ](http://docs.sphinx-users.jp/) 形式に変更するものになります。

> 現状では Mendeley から論文情報を取得し、 rst に変換するのみです。
> そこで得られた pdf から 本文の内容も rst に変更することを目標にしていますが、
> pdf 解析器の精度を向上中につき、この機能は停止しています。

## Requirement ##

見ての通り python スクリプトなので python 環境が必要です。
これは 2.7 を使用して作成しています。

また、Mendeley に関しては web アプリを参照することはしていないので、
デスクトップ版を導入する必要があります。

この際、Unix 系では ~/.local/share/data/Mendeley\ Ltd./Mendeley\ Desktop/ 以下に "<ユーザー名>@www.mendeley.com.sqlite" という sqlite のデータベースが置かれるはずです。
現状はこの db を決め打ちで読み込むようにしているので、そのように注意してください。

その他数点の python ライブラリが必要になります。

- pandas: DB アクセス及びデータ操作
- request: http アクセス
- Beautiful Soup: xml 解析
- PDFminer: pdf 解析

## Usage ##

現状では python 上で対話的に操作をする感じです。
このレポジトリが存在するローカルディレクトリで、
以下のように操作を行います。

~~~~
>> from mendeley_manager import Mendeley_Maneger
>> m = Mendeley_Maneger()
>> a = m.get_arrticle('hogehoge')
>> a.to_rst('foofoo')
~~~~

ここで、 hogehoge は rst 化したい論文の名前、 foofoo は rst ファイルをおくディレクトリです。
出力先に指定するのが ディレクトリであることに注意をしてください。
ここで指定されたディレクトリの直下に一つのディレクトリと、一つのファイルが作成されます。

- main.rat
- fig

main.rst は pdf を解析した結果出力される rst ファイルです。
もう一方の fig ディレクトリは、 pdf に画像が存在する場合、(抽出できたら) 取り出し保存します。
現状ではこの保存先に関しても自動で main.rst 内に反映するようにしています。


## To DO ##

- 変換器の精度向上
    - pdf から ヘッダー の抽出
    - pdf から まともな文章 の抽出
    - 引用の付与
    - 数式を math jax 形式に変更
    - 表の反映
    - 図の適切な配置
- 不要な 2 バイト文字を変更
- 検索用の関数作成
