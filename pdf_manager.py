# -*- coding: utf-8 -*


class PDF_Manager(object):
    """
    To Convert the pdf file to python-dict

    Usages:
        >> p = PDF_Manager()
        >> pdf_text = p.load(filename, imagewriter='./fig')
        >> # or
        >> p = PDF_Manager(filename, imagewriter='./fig')
        >> p.pddf_text  # This is the result for analyzing the pdf
    """
    def __init__(self, filename=None, imagewriter=None, passwd=''):
        from pdfminer.layout import LAParams
        from pdfminer.pdfinterp import PDFResourceManager
        from pdfminer.converter import XMLConverter
        # input option
        self.pagenos = set()
        self.maxpages = 0

        # output option
        self.outfile = None
        self.outtype = None
        self.rotation = 0
        self.layoutmode = 'normal'
        self.codec = 'utf-8'
        self.pageno = 1
        self.scale = 1
        self.caching = True
        self.showpageno = True
        self.laparams = LAParams()

        # setting pdfminer
        self.rsrcmgr = PDFResourceManager(caching=self.caching)
        self.device = XMLConverter

        # 図の抽出設定
        if imagewriter:
            from pdfminer.image import ImageWriter
            self.imagewriter = ImageWriter(imagewriter)
        else:
            self.imagewriter = imagewriter

        if filename:
            from re import compile
            url = compile(r'^(http:)')
            if url.match(filename):
                self.load_from_web(filename, passwd)
            else:
                self.load(filename, passwd)

    def __set_xml(self):
        from pdfminer.pdfinterp import PDFPageInterpreter
        from pdfminer.pdfpage import PDFPage
        from cStringIO import StringIO
        retstr = StringIO()
        fp = open(self.filename, 'rb')
        device = self.device(self.rsrcmgr, retstr, codec=self.codec, laparams=self.laparams, imagewriter=self.imagewriter)
        interpreter = PDFPageInterpreter(self.rsrcmgr, device)
        for page in PDFPage.get_pages(fp, self.pagenos, maxpages=self.maxpages, password=self.password, caching=self.caching, check_extractable=True):
            interpreter.process_page(page)
        xml = retstr.getvalue()
        fp.close()
        device.close()
        retstr.close()
        self.xml = xml.replace('>\n<', '><')

    def __set_textlines(self):
        from cleanning_text import XML_Analyzer
        self.pdf_text = XML_Analyzer(self.xml)

    def load(self, filename, passwd=''):
        self.filename = filename
        self.password = passwd
        self.__set_xml()        # run to convert pdf to xml
        self.__set_textlines()  # run to convert xml to dict
        return self.pdf_text
