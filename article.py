# -*- coding: utf-8 -*
"""
一つの論文を表現するためのクラス群です。
"""
from abc import ABCMeta, abstractmethod
from cleanning_text import split_sentences, split_text


class Info(object):
    def __init__(self, filepath, title, **info):
        self.title = title
        self.__filepath = filepath
        if info:
            self.year = info['year']
            self.tags = info['tags']
            self.pages = info['pages']
            self.issue = info['issue']
            self.volume = info['volume']
            self.keywds = info['keywds']
            self.authors = info['authors']
            self.abstract = info['abstract']
            self.publication = info['publication']

    def to_rst(self):
        head_size = len(self.title) + 10
        textline = [u'\n'.join([u'=' * head_size, self.title, u'=' * head_size, ''])]
        if self.authors:
            textline.append(u':Authors: {0},'.format(u' and '.join(self.authors)))
        if self.publication:
            textline.append(u':Jounal: {0} vol. {1}, no. {2}, pp. {3}, {4},'.format(
                self.publication,
                self.volume,
                self.issue,
                self.pages,
                self.year
            ))
        if self.tags:
            textline.append(u':Tags: {0},'.format(u'; '.join([x for x in self.tags])))
        if self.keywds:
            textline.append(u':Keywords: {0},'.format(u'; '.join(self.keywds)))
        if self.abstract:
            abstract = u'\n.. note::\n\n{0}'.format(
                '\n'.join([u'   ' + x for x in split_text(split_sentences(self.abstract))])
            )
            textline.append(abstract)
        textline.append(u'\n.. contents::\n')
        text = u'\n'.join(textline)
        return text

    def load_pdf(self, imageDir=None):
        from pdf_manager import PDF_Manager
        text = PDF_Manager(self.__filepath, imageDir)
        self.pdf_text = text.pdf_text

    def open(self):
        from subprocess import call
        call(['gvfs-open', self.__filepath])


class Content(object):
    __metaclass__ = ABCMeta

    def __init__(self, id, head=None, texts=None):
        self.id = id
        self.head = head
        self.texts = texts

    @abstractmethod
    def to_rst(self):
        if isinstance(self.texts, list):
            text = '\n'.join(self.texts)
        else:
            text = self.texts


class Table(Content):
    def __init__(self, id, headers, values, caption_head=None, caption_texts=None):
        super(Table, self).__init__(id, caption_head, caption_texts)
        self.headers = headers
        self.values = values

    def to_rst(self):
        ref = '.. _table_{0}\n\n'.format(self.id)
        capthon = ' '.join([self.head, text])
        capthon = '.. csv-table:: ' + capthon + '\n'
        header = '   :header: ' + ', '.join(self.headers) + '\n'
        values = '\n'.join(['   ' + v for v in self.values]) + '\n\n'
        return ref + capthon + header + values


class Fig(Content):
    def __init__(self, id, fig, caption_head=None, caption_texts=None):
        super(Fig, self).__init__(id, caption_head, caption_texts)
        self.fig = fig

    def to_rst(self):
        ref = '.. _fig_{0}\n\n'.format(self.id)
        capthon = ' '.join([self.head, text])
        fig = '.. figure:: {0}\n   :alt{1}:\n\n'.format(self.fig, capthon)
        return ref + fig + '   ' + capthon


class Annotation(object):
    def __init__(self, id, text):
        self.id = id
        self.text = text

    def to_rst(self):
        return '- ' + self.text


class Chapter(Content):
    def __init__(self, id, level=1, head=None, texts=None, figs=None, tables=None, annotations=None):
        super(Chapter, self).__init__(id, head, texts)
        self.level = level
        self.figs = figs
        self.tables = tables
        self.annotations = annotations

    def __head_to_rst(self):
        head_length = len(self.head) + 10
        if self.level == 1:
            return u'=' * head_length
        elif self.level == 2:
            return u'-' * head_length
        elif self.level == 3:
            return u'~' * head_length

    def to_rst(self):
        headline = self.__head_to_rst()
        head = '\n'.join([self.head, headline]) + '\n\n'
        texts = '\n'.join(self.texts)
        textlines = [head + texts]

        if self.figs:
            figs = '\n'.join([f.to_rst() for f in self.figs])
            textlines.append(figs)
        if self.tables:
            tables = '\n'.join([t.to_rst() for t in self.tables])
            textlines.append(tables)
        if self.annotations:
            annotations = '\n'.joint([a.to_rst() for a in self.annotations])
            textlines.append(annotations)

        return '\n'.join(textlines)


class Article(Info):
    def __init__(self, filepath, title, chapters=None, **info):
        super(Article, self).__init__(filepath, title, **info)
        self.chapters = []
        if chapters:
            self.chapters_size = len(chapters)
            for i in sorted([c.id for c in chapters]):
                for c in chapters:
                    if c.id == i:
                        self.chapters.append(c)

    def to_rst(self, dir_name=None):
        textlines = [super(Article, self).to_rst()]
        if not self.chapters:
            self.load_pdf()
        for c in self.chapters:
            textlines.append(c.to_rst())

        if dir_name:
            self.__write(dir_name, textlines)
        else:
            return '\n'.join(textlines)

    def load_pdf(self, imageDir=None):
        super(Article, self).load_pdf(imageDir)
        self.__add_capters()

    def __add_capters(self):
        for i in range(self.pdf_text.head_num):
            cap_id = i + 1
            texts = []
            for t in self.pdf_text.textlines:
                if t['cap'] == cap_id:
                    if t['role'] == 'head':
                        head = t['text']
                    else:
                        texts.append(t['text'])
            self.chapters.append(Chapter(cap_id, head=head, texts=texts))
        self.chapters_size = len(self.chapters)

    def __write(self, dir_name, textlines):
        from os import mkdir, path
        from glob import glob
        if not path.exists(dir_name):
            mkdir(dir_name)

        # imageDir = path.join(dir_name, 'fig')
        # self.load_pdf(imageDir)
        # images = [u'.. image:: {0}\n'.format(x) for x in glob(path.join(imageDir, '*'))]
        # textline.append(u'\n')
        # textline.extend([x for x in images])
        text = u'\n'.join(textlines)
        f = path.join(dir_name, 'main.rst')
        f = open(f, 'w')
        f.write(text.encode('utf-8'))
        f.close()
