# -*- coding: utf-8 -*
"""
ローカルにインストールされている Mendeley desktop のデータベースを読み込み、
python 上に再現します。

m = Mendeley_Manager()
a = m.get_article(file_name)
a.to_rst('dir_name')
"""


class Mendeley_Manager(object):
    def __init__(self):
        from os.path import expanduser, join
        from glob import glob
        self.__mendeley_dir = expanduser('~/.local/share/data/Mendeley Ltd./Mendeley Desktop/')
        self.__db_path = glob(join(self.__mendeley_dir, '*.sqlite'))[0]
        self.__set_db()

    def __set_db(self):
        from sqlite3 import connect
        from pandas.io.sql import read_sql
        con = connect(self.__db_path)
        self.__documents = read_sql('SELECT * FROM Documents', con)
        self.__tags = read_sql('SELECT * FROM DocumentTags', con)
        self.__keywds = read_sql('SELECT * FROM DocumentKeywords', con)
        self.__authors = read_sql('SELECT * FROM DocumentContributors', con)
        self.__files = read_sql('SELECT documentId, localUrl FROM Files INNER JOIN DocumentFiles ON Files.hash=DocumentFiles.hash WHERE localUrl LIKE "%pdf"', con)
        self.titles = self.__documents.title.tolist()
        con.close()

    def get_article(self, title):
        from os.path import exists
        from article import Article
        info = {}
        index = self.__documents.title == title
        id = self.__documents.id[index].tolist()[0]
        files = self.__files.localUrl[self.__files.documentId == id].tolist()
        if files:
            exist_files = []
            for f in files:
                filename = f.replace('%20', ' ').replace('file://', '')
                if exists(filename):
                    exist_files.append(filename)
            exist_files = list(set(exist_files))
            if exist_files:
                authors = self.__authors[self.__authors.documentId == id]
                authors = authors.firstNames + ' ' + authors.lastName
                info.update({
                    'filepath': exist_files[0],
                    'title': self.__documents.title[index].values.tolist()[0],
                    'year': self.__documents.year[index].values.tolist()[0],
                    'publication': self.__documents.publication[index].values.tolist()[0],
                    'volume': self.__documents.volume[index].values.tolist()[0],
                    'issue': self.__documents.issue[index].values.tolist()[0],
                    'pages': self.__documents.pages[index].values.tolist()[0],
                    'abstract': self.__documents.abstract[index].values.tolist()[0],
                    'authors': authors.tolist(),
                    'tags': self.__tags.tag[self.__tags.documentId == id].tolist(),
                    'keywds': self.__keywds.keyword[self.__keywds.documentId == id].tolist()
                })
                return Article(**info)
        else:
            print u'<Failed to get article>'
