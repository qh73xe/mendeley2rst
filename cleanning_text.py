# -*- coding: utf-8 -*
"""
テキスト操作の処理関数群
"""


def cleanning_utf8(text):
    pass


def find_sentence_ends(text):
    from re import finditer
    return [i.end() for i in finditer(r'[a-z]+\. ', text)]


def replace_new_line(text):
    index = find_sentence_ends(text)
    for i in index:
        text = text[:i - 1] + '\n' + text[i:]
    text = text.replace('et al.\n', 'et al. ')
    return text


def split_sentences(text):
    text = replace_new_line(text)
    return [x.strip() for x in text.split('\n')]


def split_text(sentences):
    result = []
    for t in sentences:
        t = t.replace(', and', ', \nand')
        t = t.replace(', but', ', \nbut')
        if '\n' in t:
            result.extend(t.split('\n'))
        else:
            result.append(t)
    return result


class XML_Analyzer(object):
    """
    Analyze xml and Convert to python dict.

    Useges:
        >> d = XML_Analyzer(xml)  # It is need to input the xml from python/pdfminer
        >> d.textlines  # This is the result to analyze the xml
        >> d.fonts      # This is the infomention of font
        >> d.sizes      # This is the infomention of font-size
        >> d.text_font  # This is a font name useing the texts
        >> d.head_font  # This is a font name useing the headers
    """
    def __init__(self, xml):
        from bs4 import BeautifulSoup
        tree = BeautifulSoup(xml, features="xml")
        self.__pages = tree.findChildren('page')
        # init the dict from xml
        self.__set_text_lines()
        self.__check_fonts()
        # update the dict
        self.__add_word_info()
        self.__add_role_text()
        self.__add_role_references()
        self.__add_role_heade()
        self.__add_caper()

    # 辞書情報の初期値決定
    def __set_text_lines(self):
        from collections import Counter
        textlines = []
        for p in self.__pages:
            textboxes = []
            for content in p.contents:
                if content.name == 'textbox':
                    textboxes.append(content)
            for i, box in enumerate(textboxes):
                if box.text.strip():
                    fonts = []
                    sizes = []
                    for text in box.textline.contents:
                        attrs = text.attrs
                        if attrs:
                            fonts.append(attrs['font'])
                            sizes.append(attrs['size'])
                    fonts = Counter(fonts)
                    sizes = Counter(sizes)
                    textlines.append({
                        'id': i,
                        'text': box.text.strip(),
                        'font': fonts.most_common(1)[0][0],
                        'size': int(float(sizes.most_common(1)[0][0])),
                    })
        self.fonts = Counter([t['font'] for t in textlines])
        self.sizes = Counter([t['size'] for t in textlines])
        self.textlines = textlines

    def __check_fonts(self):
        fonts = []
        for k, v in self.fonts.items():
            if 'med' in k.lower():
                fonts.append({'name': k, 'num': v, 'head': True})
            elif 'black' in k.lower():
                fonts.append({'name': k, 'num': v, 'head': True})
            elif 'bold' in k.lower():
                fonts.append({'name': k, 'num': v, 'head': True})
            else:
                fonts.append({'name': k, 'num': v, 'head': False})
        self.fonts = fonts

    #==================================
    # 文字列のタグ付
    #==================================
    def __add_word_info(self):
        self.__check_first()
        self.__check_last()
        self.__check_word_num()

    def __add_role_text(self):
        self.__check_text()
        self.__check_text2()
        self.__check_fl()
        self.__mage_text()
        self.__reset_id()

    def __add_role_references(self):
        self.__check_references()

    def __add_role_heade(self):
        self.__get_head_fonts()
        self.__check_head()
        self.__check_head2()
        self.head_num = 0
        for t in self.textlines:
            if t['role'] == 'head':
                self.head_num += 1

    def __add_caper(self):
        textlines = []
        chapter_id = 0
        for t in self.textlines:
            if t['role'] == 'head':
                chapter_id += 1
            t.update({'cap': chapter_id})
            textlines.append(t)
        self.textlines = textlines

    # 辞書情報の取得
    def __check_first(self):
        textlines = []
        from re import compile
        reg = compile(r'^([A-Z]+)')
        reg2 = compile(r'^([a-z]+)')
        reg3 = compile(r'^([0-9]+)')
        reg4 = compile(r'^(\W+)')
        for t in self.textlines:
            text = t['text']
            if reg.match(text):
                t.update({'first': 'upp'})
            elif reg2.match(text):
                t.update({'first': 'low'})
            elif reg3.match(text):
                t.update({'first': 'int'})
            elif reg4.match(text):
                t.update({'first': 'syn'})
            else:
                print text
                t.update({'first': 'unknown'})
            textlines.append(t)
        self.textlines = textlines

    def __check_last(self):
        textlines = []
        for t in self.textlines:
            text = t['text']
            if text[-1] == '.' or text[-1] == '?':
                t.update({'last': 'fin'})
            elif text[-1].isupper():
                t.update({'last': 'upp'})
            elif text[-1].islower():
                t.update({'last': 'low'})
            elif text[-1] in ['-', ',']:
                t.update({'last': 'con'})
            elif text[-1] in [':', ';']:
                t.update({'last': 'col'})
            elif text[-1] in [')', ']', '}', '>']:
                t.update({'last': 'srw'})
            else:
                t.update({'last': 'unknowm'})
            textlines.append(t)
        self.textlines = textlines

    def __check_word_num(self):
        textlines = []
        for t in self.textlines:
            text = t['text']
            t.update({'word-num': len(text.split())})
            textlines.append(t)
        self.textlines = textlines

    # text 領域の決定
    def __set_text_font(self):
        from collections import Counter
        text_fonts = []
        text_sizes = []
        for t in self.textlines:
            if t['role'] == 'text':
                text_fonts.append(t['font'])
                text_sizes.append(t['size'])
        self.text_font = Counter(text_fonts).most_common(1)[0][0]
        self.text_size = Counter(text_sizes).most_common(1)[0][0]

    def __check_text(self):
        textlines = []
        for t in self.textlines:
            if t['first'] == 'upp':
                if t['last'] in ['fin', 'col']:
                    t.update({'role': 'text'})
                elif t['last'] == 'upp':
                    t.update({'role': 'head?'})
                else:
                    t.update({'role': 'unknown'})
            else:
                t.update({'role': 'unknown'})
            textlines.append(t)
        self.textlines = textlines
        self.__set_text_font()

    def __check_text2(self):
        textlines = []
        for t in self.textlines:
            if not t['font'] == self.text_font or not t['size'] == self.text_size:
                t['role'] = 'unknown'
            if t['font'] == self.text_font and t['size'] == self.text_size:
                if t['role'] == 'unknown':
                    t['role'] = 'text?'
            textlines.append(t)
        self.textlines = textlines

    def __check_fl(self):
        textlines = []
        for t in self.textlines:
            if t['role'] == 'text?':
                if t['first'] == 'upp':
                    if t['last'] == 'low' or t['last'] == 'con':
                        t['role'] = 'con-str'
                    else:
                        t['role'] = 'unknown'
                elif t['first'] == 'low':
                    if t['last'] == 'fin':
                        t['role'] = 'con-end'
                    else:
                        t['role'] = 'unknown'
                else:
                    t['role'] = 'unknown'
            textlines.append(t)
        self.textlines = textlines

    def __mage_text(self):
        textlines = []
        textlines_sheach = self.textlines
        for t in textlines_sheach:
            if t['role'] == 'con-str':
                for t2 in self.textlines[t['id'] + 1:]:
                    if t2['role'] == 'con-end':
                        t['text'] = ' '.join([t['text'], t2['text']])
                        t['role'] = 'text'
                        break
            if not t['role'] == 'con-end':
                textlines.append(t)
        self.textlines = textlines

    # 参照領域の決定
    def __check_references(self):
        textlines = []
        reference = False
        for t in self.textlines:
            if reference:
                t['role'] = 'ref'
            if t['role'] == 'unknown':
                if 'reference' in t['text'].lower():
                    t['role'] = 'head'
                    reference = True
            textlines.append(t)
        self.textlines = textlines

    # ヘッダー領域の決定
    def __get_head_fonts(self):
        self.head_fonts = []
        for f in self.fonts:
            if f['head']:
                self.head_fonts.append(f['name'])

    def __check_head(self):
        textlines = []
        for t in self.textlines:
            if t['role'] == 'unknown':
                if t['font'] in self.head_fonts and t['word-num'] < 10 and len(t['text']) > 1:
                    t['role'] = 'head'
            textlines.append(t)
        self.textlines = textlines

    def __check_head2(self):
        from re import compile
        textlines = []
        reg = compile(r'^([1-9]\.)+\s[A-Z]+')
        for t in self.textlines:
            if t['role'] == 'unknown':
                if reg.match(t['text']):
                    t['role'] = 'text'
            textlines.append(t)
        self.textlines = textlines

    # id の再度割り振り
    def __reset_id(self):
        textlines = []
        for i, t in enumerate(self.textlines):
            t['id'] = i
            textlines.append(t)
        self.textlines = textlines
