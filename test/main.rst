===========================================================================================
Exploratory study of some acoustic and articulatory characteristics of sad speech
===========================================================================================

:Authors: Donna Erickson and Kenji Yoshida and Caroline Menezes and Akinori Fujino and Takemi Mochida and Yoshiho Shibuya,
:Jounal: Phonetica vol. 63, no. 1, pp. 1-25, 2006.0,
:Tags: プロポーザル; 感情音声,

.. note::

   This study examines acoustic and articulatory EMA data of two female speakers (American and Japanese) spontaneously producing emotional speech while engaged in an informal telephone-type conversation.
   A set of control data in which the speakers imitated or read the original emotional utterance was also recorded; for the American speaker, the intonation pattern was also imitated.
   The results suggest (1) acoustic and articulatory characteristics of spontaneous sad speech differ from that of read speech or imitated intonation speech, (2) spontaneous sad speech and imitated sad speech seem to have similar acoustic characteristics (high F(0), changed F(1) as well as voice quality), 
   but articulation is different in terms of lip, jaw and tongue positions, 
   and (3) speech that is rated highly by listeners as sad is associated with high F(0) and changed voice quality.

.. contents::

Original Paper
========================

Phonetica 2006;63:1–25DOI: 10.1159/000091404
Received: September 20, 2004Accepted: September 12, 2005
Exploratory Study of Some Acoustic andArticulatory Characteristics of SadSpeech
=========================================================================================

Donna Ericksona Kenji Yoshidab Caroline Menezesc Akinori FujinodTakemi Mochidae Yoshiho Shibuyaf
aGifu City Women’s College, Gifu City, Japan, bShoin University, Atsugi, Japan,cLaboratoire Parole et Langage, Aix-en-Provence, France, dNTT CommunicationScience Labs, Kyoto, Japan, eNTT Communication Science Labs, Atsugi, Japan, andfHokuriku Gakuin Jr. College, Kanazawa, Japan
AbstractThis study examines acoustic and articulatory EMA data of two female speak-ers (American and Japanese) spontaneously producing emotional speech whileengaged  in  an  informal  telephone-type  conversation.  A  set  of  control  data  inwhich  the  speakers  imitated  or  read  the  original  emotional  utterance  was  alsorecorded; for the American speaker, the intonation pattern was also imitated. Theresults  suggest  (1)  acoustic  and  articulatory  characteristics  of  spontaneous sadspeech differ from that of read speech or imitated intonation speech, (2) sponta-neous sadspeech and imitated sadspeech seem to have similar acoustic charac-teristics (high F0, changed F1 as well as voice quality), but articulation is different interms of lip, jaw and tongue positions, and (3) speech that is rated highly by lis-teners as sadis associated with high F0 and changed voice quality.
Copyright © 2006 S. Karger AG, Basel
Introduction
======================

What a speaker wishes to communicate is conveyed not only by linguistic units ofsyntactically  concatenated  phonemes  and  words,  but  by  acoustic  changes  in  thespeaker’s voice including F0, duration, intensity, tone of voice, rhythm and phrasing. Asmentioned by Eldred and Price [1958], communication involves not only ‘what’ is said,but ‘how’ it is said. Fujisaki [2004] suggests that the type of information about ‘how’ bereferred to as ‘paralinguistic information’, to be distinguished from ‘linguistic informa-tion’ (discrete, categorical, conveyed by the written language) and non-linguistic infor-mation (information not generally controlled by the speaker such as gender, age, andemotion). Parallel communication of such various types of information conveyed by thespeech signal, including affective, indexical, social, cultural and lexical information, isa tradition within linguistics, going back to Bühler [1934] and beyond. For reviews of various  studies  about  the  multicomplex  richness  of  expressive  speech,  see  e.g.,Schroeder [2004], Gobl and Ní Chasaide [2003] and Douglas-Cowie et al. [2003].
Fax ⫹41 61 306 12 34E-Mail karger@karger.chwww.karger.com
© 2006 S. Karger AG, Basel0031–8388/06/0631–0001$23.50/0Accessible online at:www.karger.com/journals/pho
Prof. Donna EricksonGifu City Women’s College, 7-1 Kitamachi HitoichibaGifu, 501-0192 (Japan) Tel. ⫹81 90 8725 7412, Fax ⫹81 58 296 3130E-Mail erickson@gifu-cwc.ac.jp
One  approach  to  studying  the  complicated  nature  of  expressive  speech  involvesassessing listeners’ perceptions of expressive speech, not as a static thing, but as some-thing continuously varying over time; not by using adjectives and scales describing theemotions/affects, but rather by asking listeners to assess dimensions of emotions, forinstance, activation (from passive to active) and evaluation (from negative to positive)using a graphical interface (such as ‘Feeltrace’), as described in e.g., Cowie et al. [2000]and Schroeder [2004]. One can then perform statistical analyses, such as principal com-ponents analysis (PCA), to determine which perceived emotional dimensions are associ-ated with which acoustic (or articulatory) characteristics. Along these lines ‘free choicelabeling’ can be used [see, e.g., Campbell and Erickson, 2004]. In this study, listenerswere auditorily presented eh (a Japanese back-channeling utterance) spoken by a singlespeaker, asked to ‘sort’ the sounds into boxes on the computer screen, and then assignlabels to the boxes according to how they perceived the sound. A PCA was then done inorder to determine the underlying relationships of the responses.
Another frequently used approach to studying expressive speech is to examine theacoustic and articulatory characteristics associated with a particular emotion/affectiveexpression using emotional adjectival labels such as happy, sad or angry, which areassigned by listeners or experimenters. Various studies have reported acoustic changesin  connection  with  particular  emotions/affects  [see  e.g.,  review  articles  by  Scherer,2003; Gobl and Ní Chasaide, 2003; Erickson, 2005].
Articulation  also  varies  with  affect.  For  instance,  Maekawa  et  al.  [1999]  andErickson et al. [2000] showed that tongue and jaw position changed when the speakerwas asked to produce the same utterance under different paralinguistic conditions, i.e.,suspicion, admiration or anger.
Distinguishing between acted emotions, i.e., speakers producing a specific emotionor  attitude  on  demand,  and  spontaneous  expression  of  emotions  is  important. Actedemotions most likely belong to a different category from those emotions being experi-enced by the speaker since the actor controls the verbal expression in order to convey acertain affect to the listener, whereas the person who is experiencing the emotion whilespeaking does not have the same type of control. For example, during extreme grief, thespeaker may be crying while attempting to speak. Crying is not part of the linguisticmessage per se. The speaker is not generally in control of crying. It happens in spite ofthe  speaker’s  intentions,  most  likely  the  result  of  physiological  emotionally  inducedchanges in the brain. Studies such as that by Brown et al. [1993] showed hormonal changesas the result of sadness or elation. Because the biological state of the speaker changes asa  consequence  of  certain  emotions,  such  as  sadness or  elation,  laryngeal  and  supra-laryngeal articulation may change also. Preliminary studies by Erickson et al. [2003a,b,2004a–c] suggested that acted and spontaneously expressed emotions are produced withdifferent patterns of jaw and tongue articulations. Schroeder et al. [1998], in examiningaudio-visual characteristics of amused speech, found that listeners were able to perceivegenuine expressions of amusement as different from those of imitated ones.
Expressions of certain types of strong emotions by a speaker, such as crying while atthe same time forcing oneself to speak, are interesting to study because this opens a win-dow for thinking about possible physiological connections between emotion and speech,to what extent emotion and speech are related, and to what extent strong emotion mayinterfere with or enhance linguistic tasks in terms of both production and perception.
2
Phonetica 2006;63:1–25
Erickson/Yoshida/Menezes/Fujino/Mochida/Shibuya
Sad SpeechAccording to Scherer [1979], there may be at least two kinds of sad speech: (1)sad, quiet, passive speech and (2) active grief of the type sometimes experienced inmourning. The former may or may not be classified as paralinguistic information; thelatter would be a clear example of nonlinguistic emotion. It is associated with emo-tional  arousal  accompanied  by  physiological  changes  such  as  tears. The  emotion  ofactive grief probably will affect the motor coordination involved in the control of thevocal fold vibration patterns, as well as in supraglottal articulatory movements.
The  acoustic  characteristics  of  sad quiet  speech  have  been  much  studied. Theyinclude  low  F0 [e.g.,  Iida,  2000;  Eldred  and  Price,  1958],  low  intensity  voice  [e.g.,Eldred and Price, 1958], increased duration [e.g., Iida, 2000; Eldred and Price, 1958], andvoice quality changes, such as change in the spectral energy distribution [e.g., Schereret  al.,  1991]  and  increased  breathiness.  Increased  breathiness  is  associated  with  anincreased open quotient of the glottal cycle, which is reflected in terms of high glottalamplitude quotient (AQ) [Mokhtari and Campbell, 2002], increased amplitude of thefirst harmonic relative to the other harmonics [e.g., Hanson et al., 2001], as well as asteep spectral slope [e.g., Childers and Lee, 1991].
The  acoustic  characteristics  of  the  type  of  sadness involving  active  grieving/mourning have not been studied so well, probably because it is very difficult to makerecordings  of  this  type  of  emotional  information.  However,  an  acoustic  study  ofRussian laments was done by Mazo et al. [1995]. Laments are a type of intonation usedin some Russian villages to express a feeling of grief which involves, in addition to reg-ular singing, sobs, excited exclamations, speech interruptions, sighs, and voiced breath-ing. The acoustic characteristics include high F0, changes in duration, increased energybetween 1,500 and 4,500 Hz, and vocal fold perturbations, such as vocal fry, diplophonia,and simultaneous amplitude/frequency modulation.
Articulatory characteristics of nonlinguistic emotional information have not beenextensively examined, if at all; articulation of paralinguistic information has been stud-ied  somewhat.  For  instance,  the  jaw  is  lower  for  irritation in  American  English[Menezes, 2003; Mitchell et al., 2000; Erickson et al., 1998]; the tongue dorsum is for-ward for suspicion and more back for admiration, both in American English [Ericksonet al., 2000] and Japanese [Maekawa et al., 1999]. The acoustic consequences of thedifferent tongue dorsum positions are that F1 and F2 are more raised for suspicion, morelowered for admiration, as well as more lowered for disappointment [e.g., Maekawaet al., 1999].
Our aim here is to examine the acoustic and articulatory characteristics of sponta-neous  emotions. The  current  paper  [an  extension  of  earlier  work  by  Erickson  et  al.,2003a,b  and  2004a–c]  is  an  innovative  approach  to  combining  intrusive  articulatorymeasures (EMA) while recording speech of people experiencing strong, genuine emo-tion. Comparisons of the acoustic and articulatory characteristics of the spontaneoussad speech are made for each speaker with the same words under two (or three) otherconditions: spontaneous emotion vs. imitated emotion vs. imitated phrasing/intonationvs. read speech. The reason sad speech was examined was because this was the strongemotion being experienced at that time by these speakers.
Specifically, we examine the acoustic characteristics of F0, formant frequencies,duration, and voice quality, along with the articulatory measures of tongue, lips and jawpositions. We ask (1) what are the characteristics of spontaneous emotional expressionsas compared with reading the same linguistic content.
Sad Speech
Phonetica 2006;63:1–25
3
In addition, we ask (2) whether there are differences in acoustic and articulatorycharacteristics of spontaneous sadness vs. imitated sadness. ‘Imitated’ sadness is simi-lar to ‘acted’ sadness, but with imitated  sadness, the speaker is asked to imitate theemotional utterance he/she uttered spontaneously as accurately as possible while lis-tening to a recording of it. A previous study on imitated speech [Erickson et al., 1990]showed  that  speakers,  for  instance,  were  able  to  imitate  the  F0 pattern  of  their  ownspontaneous utterances but not the voice quality characteristics. Eliciting imitated emo-tion is a way of assessing characteristics which the speaker is sensitive to, but actuallymay be different from what was present in the acoustic signal of the spontaneous utter-ances. Thus, this approach provides yet another way of glimpsing some of the acousticcharacteristics of emotional speech. Acted speech might be seen as a stereotyped codethat people have in their repertoire to convey specific emotions, which may or may notbe different from imitated emotional speech or spontaneous emotional speech.
A third question (3) we ask is whether there are acoustic and articulatory differ-ences when the speaker imitates the spontaneous sad utterance vs. when she imitatesonly the phrasing and intonational patterns of the spontaneous sad utterance.
Also, we ask (4) what are the acoustic and articulatory characteristics of speech that israted by listeners as very sad? Is this the same or different from spontaneous sad speech?In addition, we ask (5) whether there are common characteristics in the productionof emotional speech across different languages, e.g., American English and Japanese.On  one  hand,  we  might  expect  similarities  because  human  beings  experience  basicemotions; however, we also would expect differences because of the complex interplaybetween the experience of emotion and the socio-linguistic constraints on the expres-sion of emotion.
A sixth question we ask is (6) what are the similarities/differences between per-ception of sad speech in diverse languages, such as American English and Japanese?Do listeners of different languages pay attention to different acoustic characteristics inperceiving whether an utterance is sad or not?
The  strengths  of  the  study  lie  particularly  in  the  joint  acoustic  and  articulatorytreatment of the speech data and in the comparison of spontaneous and imitated emo-tion together with prosodic imitation without emotional expression. This type of studyhas not been done previously and as such, is a pioneering piece of work, presented hereas a pilot study, or ‘proof of method’. The weaknesses are those inherent in the non-laboratory design of the experiment: the linguistic content in terms of words/vowelsanalyzed is not strictly controlled nor is the timing of the collection of the spontaneousemotion  for  the  speakers  exactly  the  same,  nor  is  the  language  –  one  speaker  isAmerican, one is Japanese, albeit both are female speakers. We acknowledge the tenta-tive nature of the results, which we hope can be used as a baseline or extension for fur-ther, more extensive work.
Methods
=================

Data RecordingAcoustic  and  articulatory  data  were  recorded  (2D  EMA  system,  NTT  Research  Laboratories,Atsugi, Japan) for an American (Midwest dialect) female speaker and Japanese (Hiroshima) femalespeaker. Since a primary purpose of this study was to establish a method for recording and analyzingspontaneous emotional speech, the experimental conditions for the two speakers were not exactly thesame. Table 1 summarizes the various experimental differences in terms of speakers/languages, data
4
Phonetica 2006;63:1–25
Erickson/Yoshida/Menezes/Fujino/Mochida/Shibuya
Table 1. Summary of experimental method: speakers/languages, data sets, sessions, timing of sessions,utterance conditions, vowels examined and articulatory/acoustic measurements
Speaker/language
Sets ofdata
Sessions Sessiontiming
Conditions Word/vowel
Articulatory Acousticmeasuresmeasures
2
American  2 sets: Set 1:English
informal, spontaneous conversations;Set 2: controldata
Japanese
2 sets: Set 1:  1informal, spontaneous conversation; Set 2: control data
1 month prior 4 conditions: to mother’s spontaneous death (on day emotion, of operation imitated for pancreatic emotion, imitatedcancer) and intonation,5 monthsreadlater4 months 3 conditions:  kara /a/ after mother’s spontaneous sudden death due to brain aneurysm
emotion, imitated emotion, read
Jx, Jyleave /i/(2 ULx, ULyutterances) LLx, LLyT1x, T1yT2x, T2yT3x, T3y
Jx, Jy(2 ULx, ULyutterances) LLx, LLyT1x, T1yT2x, T2y
duration,F0, F1, F2andamplitudeof glottalopening(AQ)
duration,F0, F1, F2,andspectral tilt
sets,  sessions,  timing  of  sessions,  utterance  conditions,  vowels  examined  and  articulatory/acousticmeasurements.
For both speakers, two sets of data were recorded. Set 1 was done as an informal spontaneous tele-phone dialogue with another speaker through an earphone/microphone set-up, where the other speakersat in a separate room. In the American case, the other speaker was the third author, and friend/colleagueof the speaker; in the Japanese case, the other speaker was the sixth author. The other speaker, i.e., con-versation partner, asked the subject various unrehearsed questions based on a list of topics related to thesubject’s personal life to evoke happiness, sadness or anger. The timing of the experiment was fortunatefor collecting sad (grieving) emotions (including crying while speaking), since in the American case,the subject had just recently learned that her mother had been diagnosed with a fatal untreatable disease,and in the Japanese case, the subject had recently lost her mother due to a brain aneurysm. The conver-sation partner was aware of the recent sad situation, and much of the interview focused on this. The dia-logue continued in a natural manner, while EMA recordings were made within a window frame of 20 s,with a break in recording of about 3 s between frames. Acoustic recording, however, was continuous.Video recording was also done but not used for systematic analysis.
Set 2 utterances consisted of control data for the initial data recordings. The particular utterancesfor the control study were selected because these were the ones during which the speaker was cryingwhile speaking. However, as shown in table 1, for the American English speaker, the recordings weremade in a second session 5 months later; for the Japanese, the recordings were made in the same ses-sion about an hour later, after the subject had finished reading a list of sentences for a different exper-imental protocol. Two sessions had the advantage of providing enough time to select the best controldata from the emotional speech, but the disadvantage that the EMA coordinates for the articulatorymeasurements differed by 5 degrees between the two sessions. Mathematically transforming the datafrom  one  session  to  match  the  coordinates  of  the  other  proved  unproblematic,  whereas  insufficienttime to select data may continue to pose problems for experimenters. It is recommended that futurework of this type gathers the naturally occurring emotional speech in a separate session from the con-trol conditions.
Both speakers were asked to reproduce the original utterances in the following ways as shown intable 1. Notice that for the American speaker there were four conditions, whereas there were only threefor the Japanese speaker. The reason for this is explained in item 2 below.
Control Condition 1 (Imitated Emotion). Imitating (i.e., shadowing) the wording, phrasing, into-nation and emotion of the original utterances while at the same time listening to a taped recording
Sad Speech
Phonetica 2006;63:1–25
5
through  headphones,  and  looking  at  a  transcript  (which,  in  the  American  English  case,  had  beenmarked indicating phrasing and intonation patterns of the original utterances). The imitated emotionalutterances (IE) were repeated 3 times for the American English speaker, and 6 times for the Japanesespeaker. For analysis purposes, only words from the first three repetitions were examined. Since theutterances were rather long (20 s), the speaker also saw a copy of the text while imitating the utterances.
Control Condition 2 (Imitated Intonation). For the American speaker only, imitating (i.e., shad-owing) just the wording, phrasing and intonation (but not emotion), also while listening to the originalutterance and looking at the transcript. The imitated intonation utterances (II) were repeated 3 times.The American speaker had considerable training in phonetics and was able to do this task easily, butthe task was too difficult for the Japanese speaker, especially since the experiment was done in one ses-sion, and there was no time to coach the speaker.
Control Condition 3 (Read). Read the original utterance from the orthographic transcript of theoriginal recording. (For Japanese, the transcript was in the Japanese writing system.) The read utter-ances (R) were repeated twice for the American English speaker and 6 times for the Japanese speaker.Only words from the first three repetitions were used for the analysis.
UtterancesIdeally, we would want to keep the vowel context the same across the two languages; however,leave and kara (‘because’) were chosen because these were the instances during which the speakersshowed the strongest sad emotion and there were no instances in which both speakers showed strongsadness in the same vowel context.
For the American English sad speech, we analyzed instances of the word leave, as uttered twicein a single 20-second utterance, under four utterance conditions: (E) spoken in the first experiment asthe speaker was grieving for her mother (i.e., actually sobbing); (IE) imitating the phrasing, intonationand emotion while listening to a taped recording of the original utterance; (II) imitating only the phras-ing and intonation, also while listening to the original utterance; (R) reading the original utterance,keeping the phrasing and intonation as much the same as in the original recording. In total, there were16 recordings of leave: 2 E utterances, 6 IE utterances, 6 II utterances, and 2 R utterances.
For the Japanese sad speech, we analyzed instances of the word kara as it occurred in two utter-ances under three-utterance conditions: E (tears were running down the speaker’s cheek1 as she wassaying ‘kara’), IE, and R. In total, there were 14 recordings of kara: 2 E utterances, 6 IE utterances,and 6 R utterances.
Articulatory Analysis MethodWe  examined  the  movement  of  the  EMA  receiver  coils  attached  to  the  (1)  lower  incisor(mandible) (2) upper lip, (3) lower lip, and (4) receiver coils (T1, T2, T3, T4) attached along the longi-tudinal  sulcus  of  the  speaker’s  tongue.  The  positions  of  the  transmitter  coils  determine  thecoordinate system [Kaburagi and Honda, 1997] with the origin positioned slightly in front of and belowthe chin. All EMA values are positive, with increasingly positive y-values indicating increasingly raisedjaw or tongue position, and increasingly positive x-values, increasingly retracted jaw or tongue position.The  coordinates  of  the American  English  speaker’s  first  recording  were  transformed  by  5  degrees,which was the measured difference between the coordinates in the two sessions [Erickson et al., 2003b].Articulatory measurements were made for the x-y coil positions for the upper and lower lip (UL,LL), for the mandible (J), and the tongue (T1, T2, T3, T4) at the time of maximum jaw opening for theutterance, using a MATLAB-based analysis program. Note that T4 recordings were not reliable foreither of the speakers, and T3 was not reliable for the Japanese speaker because of coil-tracking prob-lems. For the American English utterances, articulatory as well as acoustic measurements were madeat  the  time  of  maximum  jaw  opening  for  leave.  For  the  Japanese  utterances,  articulatory  measure-ments were made at the time of maximum jaw opening during the second mora in the utterance kara.
1As seen from the video images, for the Japanese speaker, the manifestation of crying was tears running down thecheeks; for the American speaker, the face was contorted in a sobbing posture with jaw open and eyebrows knittogether, as well as tears.
6
Phonetica 2006;63:1–25
Erickson/Yoshida/Menezes/Fujino/Mochida/Shibuya
E81.3 kara
xJ
yJ
xLU
yLU
xLL
yLL
x1T
y1T
…
x2T
y2T
2.2
2.25
2.3
2.35
2.4
2.45
2.5
2.55
2.6
2.65
2.7
Fig. 1. Sample articulatory data of kara from utterance 81 (E). The bottom trace is the acoustic wave-form. Vertical lines indicate lowest Jy position in each mora. Articulatory measurements made duringthe second mora were used in the analysis. The x-axis shows time in seconds. The values of the tickmarkings for the y-axes are as follows: Jx 7.85–7.80 mm; Jy 12.8–12.6 mm; ULx 5.9–5.85 mm; ULy15.2–15.15 mm;  LLx  6.45–6.3 mm;  LLy  13.6–13.3 mm;  T1x  9.2–8.8 mm;  T1y  14.8–14.0 mm;  T2x10.2–9.8 mm; T2y 20–14.5 mm.
A sample of a data file is shown for Japanese in figure 1. Notice there are two distinct jaw openings,one for each mora. However, for two of the utterances (one IE and one R), there was only one jawopening, which occurred at the plosion release for /k/.
Acoustic Analysis MethodFor both the American English and Japanese data, we measured duration, F0, F1 and voice quality.For American English, as part of an earlier collaborative study with Parham Mokhtari [see Ericksonet al., 2003b], we were able to examine the AQ of the glottal opening as estimated from the derivative ofthe glottal flow, as described below. However, we did not have access to this technique for the Japaneseanalysis, so we used a simpler technique of examining the spectral tilt, also described below.
American English. Acoustic analysis of the word leave – duration, F0, F1 and glottal openingmeasures  (AQ)  –  was  done  using  the  methods  of  inverse-filtering  and  phonation-type  estimationreported in Mokhtari and Campbell [2003]. In our study we focus on the F0, F1 and AQ results obtainedspecifically at the time of maximum jaw opening within the syllable nucleus, as determined from thesimultaneous EMA measurements.
Linear-prediction (LP) cepstra measured at each analysis frame were used to obtain an initialestimate of the first 4 formant frequencies and bandwidths, by way of the linear cepstrum-to-formantmapping proposed by Broad and Clermont [1986]. Although the mapping had been trained on a subsetof carefully measured formants of a female speaker of Japanese [Mokhtari et al., 2001], it was foundto yield remarkably reasonable results for the American female speaker, as judged by visual inspectionof the formant estimates superimposed on spectrograms. These formant estimates were then refined ateach  frame  independently,  by  an  automatic  method  of  analysis-by-resynthesis  whereby  all  8  para-meters (4 formant frequencies and bandwidths) are iteratively adjusted in order to minimize a distance
Sad Speech
Phonetica 2006;63:1–25
7
between  a  formant-generated  (or  simplified)  spectrum  and  the  original  FFT  spectrum  of  the  sameanalysis frame. The optimized formants centered around the point of maximum jaw opening in eachutterance of leave were then used to construct time-varying inverse filters with the aim of eliminatingthe effects of the vocal-tract resonances (or formants). The speech signal was first high-pass filtered toeliminate low-frequency rumble (below 70 Hz), then low-pass filtered to eliminate information abovethe fourth formant, and finally inverse filtered to eliminate the effect of the first 4 vocal-tract resonances.The resulting signal (52 ms) was integrated to obtain an estimate of the glottal flow waveform.
The glottal AQ, proposed independently by Fant et al. [1994] and Alku and Vilkman [1996] isdefined as the peak-to-peak amplitude of the estimated glottal flow waveform divided by the amplitudeof the minimum of the derivative of the estimated flow. It is a relatively robust, amplitude-based meas-ure which gives an indication of the effective duration of the closing phase of the glottal cycle. As dis-cussed by Alku et al. [2002], AQ quantifies the type of phonation which is auditorily perceived alongthe continuum from a more pressed (i.e., creaky) to a more breathy voice quality.
Japanese. Acoustic analyses of the /ra/ mora – duration, spectral tilt, F0, and F1 (around the mid-portion of the /ra/ mora in kara) – were done using Kay Multispeech 3700 Workstation. We tried ordi-nary methods to characterize the creakiness of the voices, e.g., comparison of H1 with H2 or H3 [NíChasaide and Gobl, 1997]. However, because of the irregular glottal pulsing in nonmodal phonation anextra set of harmonics appear parallel to F0 and its harmonics [Gerratt and Kreiman, 2001], and it wasnot easy to reliably resolve each harmonic. However, visual inspection of the FFT results reveals thatspectral differences of the three categories of speech reside in the strength of the energy around F1, i.e.,600–1,000 Hz, with R having the strongest energy and IE, the weakest. Therefore we devised a methodto capture the gross characteristics of the harmonics. A regression analysis was made for each FFTresult (512 points) from H1 to 1,000 Hz, with the slope of the regression line taken as an index of spec-tral tilt. Fitting a single regression line to spectrum was explored in Jackson et al. [1985].
Perceptual AnalysisThe words used in the acoustic/articulatory analyses (described in ‘Utterances’ above) were pre-sented for auditory judgment in randomized order 4 times with HDA200 Sennheiser headphones in aquiet room, using a G3-Macintosh computer and Psyscope software. The task was to rate each wordaccording to the perceived degree of sadness (5-point scale; 5, saddest). A practice test of 6 utterancespreceded the test. For the American English test, the 16 instances of leave were presented auditorily to11 American college students (4 males, 7 females) at The Ohio State University; for the Japanese test,the 14 instances of kara were presented auditorily to 10 Japanese female college students at Gifu CityWomen’s College.
Results
=================

Perception Test ResultsFor both American English and Japanese, listeners ranked emotional and imitatedemotional  speech  as  sadder than  read  speech,  and  in  the  case  of American  English,emotional and imitated emotional speech as sadder than imitated intonation speech, ascan be seen in figure 2.
One-way ANOVA  of  the  perceptual  ratings  (mean  for  all  the  listeners)  with  theutterance conditions (4 levels: E, IE, II, R for American English and 3 levels: E, IE, R forJapanese) as an explanatory variable found significant main effects for both languages(American  English,  F(3,12) ⫽ 351.328,  p ⬍ 0.000;  Japanese,  F(2,11) ⫽ 14.704,p ⬍ 0.001).  The  Bonferroni  pairwise  comparisons  revealed  that  perceptual  rating  ofsadness is significantly higher for E and IE compared to II and R for American English(p ⬍ 0.000), and for IE compared to R for Japanese (p ⫽ 0.001). The finding that imi-tated intonation in American English was not rated as sad as spontaneous or imitated various  studies  about  the  multicomplex  richness  of  expressive  speech,  see  e.g.,Schroeder [2004], Gobl and Ní Chasaide [2003] and Douglas-Cowie et al. [2003].
8
Phonetica 2006;63:1–25
Erickson/Yoshida/Menezes/Fujino/Mochida/Shibuya
5
4
3
2
1
sgnitaR
a
E
IEIICategory
R
5
4
3
2
1
sgnitaR
b
E
IE
R
Category
Fig. 2. Ratings by American listeners of the perceived degree of sadness of 16 utterances of leave(left side) and ratings by Japanese listeners of perceived degree of sadness of 14 utterances of kara(right side). The error bars indicate standard deviation of p ⫽ 0.68.
Imitated expressions of sadness (IE) were actually rated as slightly sadder thanspontaneous  expressions  of  sadness (E)  for  both  American  English  and  Japanese.However, the differences were not significant. A larger sample size is necessary to fur-ther explore this.
Acoustic Analysis ResultsFigure 3 shows the results of the acoustic analysis of American English and Japanese
Figure  3  shows  that  F0,  F1 and  voice  quality  (but  not  duration)  of  imitated  sadspeech  are  similar  to  those  for  spontaneous  sad speech.  One-way  ANOVA  of  theacoustic measures (F0, F1, voice quality, and duration) with the utterance conditions (E,IE, II, R for American English and E, IE, R for Japanese) as an explanatory variablefound main effects for duration, F0 and AQ for American English and for duration andF0 for Japanese (table 3A in the Appendix). Bonferroni pairwise comparisons found nosignificant differences between the E and IE conditions for F0, duration and voice qual-ity for either the American English or Japanese. Both speakers showed a higher F0 forsad and  imitated  sad speech  than  for  read  speech  (and  imitated  intonation  speech).Bonferroni pairwise comparisons found F0 was significantly higher for E and IE com-pared to II and R for American English (p ⬍ 0.000), and for E and IE compared to R(p ⫽ 0.013 and p ⫽ 0.038, respectively) for Japanese. The finding of high F0 (whichwas clearly audible) is different from that usually reported for sad quiet speech, butsimilar to what was found for the active grieving seen in Russian laments.
F1 values were similar for spontaneous and imitated sad compared to those for read(and imitated intonation) speech: for the American English speaker (for the vowel /i/), sadness suggests that intonation itself is not sufficient to convey sadness; as discussed in‘Results  of  Pearson  Correlation  with  Ratings  of  Sadness  and Accoustic/ArticulatoryMeasures’ below, voice quality and F0 height are salient characteristics of sadness.
Sad Speech
Phonetica 2006;63:1–25
9
500
400
300
200
100
400
300
200
100
400390380370360350340330320310300
1.61.51.41.31.21.11.00.90.80.70.6
)sm
(
noitaruD
a
)zH
(
0F
b
)zH
(
1F
c
)sm
(
QA
d
E
IEIICategory
R
E
IEIICategory
R
E
IEIICategory
R
E
IEIICategory
R
250
200
150
100
250
200
150
100
1,000
900
800
700
0.00
⫺0.01
⫺0.02
⫺0.03
⫺0.04
)sm
(
noitaruD
e
)zH
(
0F
f
)zH
(
1F
g
l
epos lartcepS
h
E
IE
Category
R
E
IE
Category
R
E
IE
Category
R
E
IE
Category
R
10
Phonetica 2006;63:1–25
Erickson/Yoshida/Menezes/Fujino/Mochida/Shibuya
Fig. 4. Sample acoustic wave forms for Japanese kara. The top panel is sad speech (E), next is imitatedsad speech (IE), and the bottom is read (R) speech. Each tick marking on the time axis indicates 100 ms.
F1 values for sad and imitated sad speech were higher, and for the Japanese speaker(for the vowel /a/), they were lower. These differences were not significant. However,the graphs suggest a tendency for the /i/ and /a/ vowels to centralize in sad speech; thisneeds further investigation. The finding of low F1 for sad and imitated sad utterancesfor the Japanese speaker is reminiscent of the finding of lowered F1 values for disap-pointment (also for the vowel /a/) reported by Maekawa et al. [1999].
In addition, both speakers showed changes in voice quality for the sad and imi-tated  sad speech,  compared  with  the  read  (and  imitated  intonation)  speech.  For  theAmerican English speaker, both spontaneous and imitated sad speech have a low AQ.Bonferroni pairwise comparisons found AQ significantly lower for E and IE comparedto  II  (E ⬍ II:  p ⫽ 0.018;  IE ⬍ II:  p ⬍ 0.000),  and  for  the  Japanese  speaker,  a  steepspectral tilt (but no significant differences).
The finding of low AQ for the American English spontaneous sad utterances is dif-ferent from the high AQ previously reported for sad, quiet breathy speech. It may be thatlow AQ is seen in active grieving because crying would probably involve muscular ten-sion, including tension of the vocal folds. When vocal folds are tense/pressed, the dura-tion of vocal fold closure is large, and this would lower AQ.
As for the Japanese, spontaneous sad speech was perceived by the authors to bebreathy-voiced and the imitated sad, creaky-voiced, and this can be seen in the acousticwaveforms in figure 4. The spontaneous sad speech in the top panel of the figure shows various  studies  about  the  multicomplex  richness  of  expressive  speech,  see  e.g.,Schroeder [2004], Gobl and Ní Chasaide [2003] and Douglas-Cowie et al. [2003].
Fig. 3. Results of acoustic measurements, both for American English leave (left side) and Japanese kara(right side) for each of the utterance conditions. From top to bottom, duration, F0, F1, AQ (for AmericanEnglish) and spectral slope (for Japanese). The error bars indicate standard deviation of p ⫽ 0.68.
Sad Speech
Phonetica 2006;63:1–25
11
15.0
14.8
yLU
14.6
14.4
14.2
13.2
13.0
a
12.8
yLL
12.6
12.4
12.4
b
12.2
yJ
c
12.0
14.4
14.2
14.0
y1T
13.8
13.6
15.2
15.0
14.8
14.6
15.615.415.215.014.814.614.414.214.0
d
y2T
e
y3T
f
15.6
15.4
15.2
15.0
13.6
13.4
yLU
g
13.2
yLL
13.0
12.8
12.9
h
y 12.7
J
12.5
14.7
14.5
14.3
14.1
13.9
13.7
15.3
15.1
14.9
14.7
14.5
14.3
i
y1T
j
y2T
k
5.3
5.5
5.7ULx
5.9
6.1
6.1
6.3
6.5
6.7
LLx
7.6
7.8
8.2
8.4
8.0Jx
8.5
8.7
8.9 9.1 9.3T1x
9.5 9.7
9.9
9.8
10.0T2x
10.2
EIER
EIER
EIER
EIER
EIER
5.8
6.0
6.2
6.6
6.8
7.0
6.4ULx
5.6
5.8
6.0
6.2
6.4
6.6
LLx
6.9
7.1
7.3
7.5
Jx
7.7 7.9
8.1
8.3
8.5
8.7
T1x
8.5
8.7
8.9
9.5
9.7
9.9
9.39.1T2x
EIEIIR
EIEIIR
EIEIIR
EIEIIR
EIEIIR
EIEIIR
9.7
10.19.9
10.4
10.6
10.8
11.4
11.7
11.9
12.1
12.3
11.011.2T3x
12
Phonetica 2006;63:1–25
Erickson/Yoshida/Menezes/Fujino/Mochida/Shibuya
The  averaged  durations  of  the  imitated  sad  speech  for  the  American  Englishspeaker were generally longer than those of the spontaneous sad speech, as well as theother conditions. However, the only significant difference was for IE compared to II(p ⫽ 0.007). Perhaps the reason the imitated sad speech was longer than the sponta-neous sad speech was because the speaker was expecting sad speech to be slow, andmay have inadvertently allowed this expectation to influence her production.
For the Japanese speaker, the spontaneous and imitated sad utterances tended to belonger  than  the  read  utterances  and  significantly  longer  for  E  compared  to  R(p ⫽ 0.023), which is consistent with what is reported in the literature for sad speech.
Articulatory Analysis ResultsFigure  5  shows  the  averaged  horizontal  and  vertical  coil  positions  for  theAmerican English and Japanese speakers. Whereas the acoustic characteristics of spon-taneous  and  imitated  sadness were  fairly  similar,  the  articulatory  characteristics  ofthese  two  conditions  are  different:  imitated  sad  speech  tends  to  be  similar  to  readspeech, or imitated intonation, rather than to spontaneous sadness. The averaged artic-ulatory values are shown in tables 4A and 5A in the Appendix. Table 2 summarizes thesignificantly different characteristics (p ⬍ 0.05 based on Bonferroni pairwise compar-isons) between sad and imitated sad speech.
For  the American  English  speaker  for  spontaneous  sadness compared  with  theother conditions (fig. 5 left side), the upper lip and lower lip are retracted, the lower lipis raised, the jaw is lowered, the tongue tip is fronted, the tongue blade is fronted andthe tongue dorsum is fronted and raised. One-way ANOVA of the articulatory measures various  studies  about  the  multicomplex  richness  of  expressive  speech,  see  e.g.,Schroeder [2004], Gobl and Ní Chasaide [2003] and Douglas-Cowie et al. [2003].
Fig. 5. Results of articulatory x-y measurements in millimeters for UL, LL, J and T1, T2, and T3.American English leave is on left-side, and Japanese kara is on right side. Smallest y-axis values indi-cate lowest coil positions, and smallest x-axis values indicate most forward coil positions, so that thelower left corner of the graphs indicate the lowest, most forward position of the articulators (as if thespeaker were facing to the left of the page).  E indicates sad speech, IE, imitated sad speech, II, imi-tated intonational speech, and R, read speech.
Sad Speech
Phonetica 2006;63:1–25
13
Table 2. Articulatory characteristics of sad speech compared to imitated sad speech
Upper lip
Lower lip
Jaw
Tongue tip
Tongueblade
Tonguedorsum
Spontaneoussad speech
Imitated sadspeech
retractedfor Am. Eng.; retracted, lowered for Japanese
protrudedfor Am. Eng.; protruded, raised for Japanese
retracted,raised forAm. Eng.;retracted,lowered for Japaneseprotruded,lowered forAm. Eng.;protruded,raised for Japanese
retracted,fronted forlowered for  Am. Eng.;fronted forAm. Eng.;protruded,Japaneselowered forJapaneseprotruded,raised forAm. Eng.;retracted,raised forJapanese
backed forAm. Eng.;backed forJapanese
fronted forAm. Eng.
fronted,raised forAm. Eng.
backed for backed,Am. Eng.
lowered forAm. Eng.
(UL, LL, J, T1, T2 and T3) with the utterance conditions (E, IE, II, R) as factors foundsignificant main effects (table 6A in the Appendix). Bonferroni pairwise comparisonsfound  ULx  more  retracted  for  E  compared  to  IE,  II,  and  R  (p ⬍ 0.000),  LLx  moreretracted for E compared to IE, II (p ⬍ 0.000), and R (p ⫽ 0.031), LLy more raised forE  compared  with  IE,  II,  and  R  (p ⬍ 0.01),  Jx  more  retracted  for  E  compared  to  IE(p ⫽ 0.005) (while IE is more protruded than II, p ⫽ 0.004 or R, p ⬍ 0.000), Jy lowerfor E compared to IE (p ⫽ 0.018), T1x more fronted for E compared to IE, II and R(p ⬍ 0.000), T1y lower for E compared to II (p ⫽ 0.025), T2x more fronted for E com-pared to IE and II (p ⬍ 0.000) and R (p ⫽ 0.002), T3x more fronted for E compared toIE,  II  and  R  (p ⬍ 0.000)  and  T3y  for  E  more  raised  compared  to  IE,  II,  and  R(p ⬍ 0.000).
That  the  American  English  speaker  showed  tongue  blade/dorsum  raising  andfronting for sad speech may be associated with the more open jaw used by this speaker,and the necessity to produce a phonologically recognizable high /i/ vowel when the jawis open [see e.g., Erickson, 2002].
For the Japanese speaker for sad speech compared to the other conditions (fig. 5right side), the upper and lower lips are retracted and lowered, the jaw is protruded andlowered (but lowered only compared with imitated sad speech, not read speech), andthe tongue tip is fronted. One-way ANOVA of the articulatory measures (UL, LL, J, T1,T2 and T3) with the utterance conditions (E, IE, R) as factors found significant maineffects (table 6A in the Appendix). Bonferroni pairwise comparisons found ULx moreretracted for E compared to IE and R (p ⬍ 0.000), ULy lower for E compared to IE andR (p ⬍ 0.000), LLx more retracted for E compared to IE (p ⬍ 0.000) and R (p ⫽ 0.009),LLy lower for E compared with IE (p ⫽ 0.009), Jx more protruded for E compared toIE and R (p ⬍ 0.000) and Jy lower for E compared to IE (p ⫽ 0.010), and T1x morefronted for E compared to IE (p ⫽ 0.003) and R (p ⫽ 0.012).
It  is  interesting  that  in  imitating  sadness,  both  speakers  showed  protruded  lips,configuring  a  lip-pouting  position.  It  is  also  interesting  that  this  gesture  is  differentfrom the one used for spontaneous sad speech, in which both speakers retracted theirlips.  It  may  be  that  sadness with  strong  crying  is  likely  to  produce  lip  retractionwhereas imitated sadness without crying need not.
14
Phonetica 2006;63:1–25
Erickson/Yoshida/Menezes/Fujino/Mochida/Shibuya
Another interesting characteristic of sad speech by the American English speakerwas the raised upper (for one of the utterances) and lower lips (see second graph leftside of fig. 5), which matches Darwin’s [1872, pp. 162–164] description of the typicalsquare-shaped  open-mouth  crying  face. According  to  Darwin,  crying  is  initiated  byknitting the eyebrows together. This maneuvre supposedly is necessary in order to pro-tect the eye during violent expiration in order to limit the dilation of the blood vessels[p. 162]. Contraction of the orbicular muscles causes the upper lip to be pulled up, andif the mouth is open, then the result is the characteristic square-shaped mouth of crying.The video image of the American English speaker while crying and saying the word‘leave’ was exactly this. The raised upper and lower lips follow from the knitted eye-brows of this speaker when crying.
Results of Pearson Correlation with Ratings of Sadness andAcoustic/Articulatory MeasuresScatter plots of sadness ratings as a function of F0, voice quality (AQ for Americanand spectral slope for Japanese), F1, and duration are shown in figure 6. High ratings ofsadness are associated with high F0 (both American English and Japanese) low F1 (onlyfor  Japanese),  and  steep  spectral  slope  (only  for  Japanese),  high AQ  (for AmericanEnglish), and increased duration (only for American English). A Pearson correlationanalysis for Japanese (using the numerical results listed in tables 2A and 5A) showed asignificant linear correlation (p ⬍ 0.01) between sadness judgments and F0 (r ⫽ 0.81),spectral slope (r ⫽ ⫺0.69) and F1 (r ⫽ ⫺0.68); however, for American English raters,no Pearson correlation could be done since there was a bimodal distribution. It is notclear  why  the American  English  speaker  showed  a  bimodal  distribution.  The  smallsample size may have contributed to the bimodal patterns.
Figure 7 shows sadness ratings and articulatory measures (LLy, T1y, and Jx for various  studies  about  the  multicomplex  richness  of  expressive  speech,  see  e.g.,Schroeder [2004], Gobl and Ní Chasaide [2003] and Douglas-Cowie et al. [2003].
American English, and LLy, LLx, and Jy for Japanese).
Japanese  listeners  displayed  a  significant  linear  correlation  between  ratings  ofsadness and raised lower lip (r ⫽ 0.76, p ⬍ 0.01), whereas American listeners showeda  bimodal  distribution,  with  low  sadness ratings  associated  with  lowered  lower  lip,raised tongue tip, and retracted jaw, and high sadness ratings associated with raisedlower  lip,  lowered  tongue  tip  and  protruded  jaw,  which  also  may  be  related  to  thisspeaker’s bimodal production of F0 values. A potential relation between supraglottalarticulation and laryngeal tension is discussed at the end of this section. For Japanese,sadness judgments showed a significant correlation (p ⬍ 0.01) with protruded lowerlip (r ⫽ ⫺0.65) and raised jaw (r ⫽ 0.76).
Table 3 summarizes the pertinent acoustic and articulatory characteristics of well-perceived sad speech. The articulatory and acoustic characteristics of well-perceivedsad American English speech are included here, since it is not clear whether the patternof  bimodal  distribution  is  an  artifact  of  the  lack  of  a  continuous  range  of  F0 valuesand/or small sample size.
We interpret the results of the correlation between listener ratings of sadness andacoustic/articulatory  measures  as  follows.  In  imitating  sad speech,  the  AmericanEnglish and the Japanese speaker successfully imitated the high F0 and changed voicequality characteristics of the spontaneous sad speech.
However, it seems that there is a difference between the way a speaker articulatesspontaneous sad speech, and what articulatory characteristics ‘convey’ sadness to a lis-tener. For both American English and Japanese, speech that was given a high rating for various  studies  about  the  multicomplex  richness  of  expressive  speech,  see  e.g.,Schroeder [2004], Gobl and Ní Chasaide [2003] and Douglas-Cowie et al. [2003].
Sad Speech
Phonetica 2006;63:1–25
15
400
300
200
100
1.61.51.41.31.21.11.00.90.80.70.6
400390380370360350340330320310300
500
400
300
200
100
)zH
(
0F
a
)sm
(
QA
b
)zH
(
1F
c
)sm
(
noitaruD
d
1
2
3
4
5
Ratings
1
2
3
4
5
Ratings
1
2
3
4
5
Ratings
1
2
3
4
5
Ratings
EIEIIR
EIEIIR
EIEIIR
EIEIIR
)zH
(
0F
250
200
150
100
e
l
epos lartcepS
f
0.00
⫺0.01
⫺0.02
⫺0.03
⫺0.04
1,000
900
800
700
250
200
150
100
)zH
(
1F
g
)sm
(
noitaruD
h
1
2
3
4
5
Ratings
1
2
3
4
5
Ratings
1
2
3
4
5
Ratings
2
4
3Ratings
5
EIER
EIER
EIER
EIER
Fig.  6. Scatter  plots  of  acoustic  measurements  and  perception  ratings  for American  English  (leftside) and Japanese (right side). E indicates sad speech, IE, imitated sad speech, II, imitated intona-tional speech, and R, read speech.
16
Phonetica 2006;63:1–25
Erickson/Yoshida/Menezes/Fujino/Mochida/Shibuya
13.113.012.912.812.712.612.512.4
14.414.314.214.114.013.913.813.7
7.4
7.3
7.2
7.1
7.0
6.9
)
mm
( yLL
a
)
mm
( y1T
b
)
mm
( xJ
c
EIEIIR
EIEIIR
EIEIIR
1
2
3
4
5
Ratings
1
2
3
4
5
Ratings
1
2
3
4
5
Ratings
13.613.513.413.313.213.113.012.9
6.6
6.5
6.4
6.3
6.2
6.1
12.9
12.8
12.7
12.6
12.5
)
mm
( yLL
d
)
mm
( xLL
e
)sm
(yJ
f
EIER
EIER
EIER
1
2
3
4
5
Ratings
1
2
3
4
5
Ratings
1
2
3
4
5
Ratings
Fig.  7. Scatter plots of articulatory measurements and perception ratings for American English (leftside) and Japanese (right side). E indicates sad speech, IE, imitated sad speech, II, imitated intonationalspeech, and R, read speech. For the vertical coil positions, the lowest value indicates lowest coil position;for the horizontal coil positions, the lowest value indicates the more forward (advanced) coil position.
Sad Speech
Phonetica 2006;63:1–25
17
Table 3. Acoustic and articulatory characteristics of well-perceived sad speech
Well-perceived sad speech
American EnglishJapanese
Lower lip
Jaw
raisedraised,protruded
protrudedraised
Tonguetip
lowered
F0
F1
Dur. Voicequality
raisedraised
lowered
long
low AQsharp spectralslope
400
300
200
100
14.414.314.214.114.013.913.8
13.7
)zH
(
0F
a
)
mm
( y1TT
b
EIEIIR
EIEIIR
0.6
0.7 0.8 0.9 1.0
1.1 1.2 1.3AQ (ms)
1.4 1.5
1.6
0.6
0.7 0.8 0.9 1.0
1.1 1.2 1.3AQ (ms)
1.4 1.5
1.6
250
200
150
)zH
(
0F
100
⫺ 0.04
13.6
13.513.413.313.213.113.012.9
⫺ 0.04
12.9
12.8
12.7
12.6
12.5
⫺ 0.04
c
)
mm
( yLL
d
)
mm
( yJ
e
EIER
EIER
EIER
0.00
0.00
0.00
⫺ 0.03
⫺ 0.02
⫺ 0.01
Spectral slope
⫺ 0.03
⫺ 0.02
⫺ 0.01
Spectral slope
⫺ 0.03
⫺ 0.02
⫺ 0.01
Spectral slope
Fig.  8. Scatter  plots  of  acoustic/articulatory  measurements  and  voice  quality  measurements  forAmerican English (left side) and Japanese (right side). E indicates sad speech, IE, imitated sad speech,II, imitated intonational speech, and R, read speech.
18
Phonetica 2006;63:1–25
Erickson/Yoshida/Menezes/Fujino/Mochida/Shibuya
Table 4. Acoustic and articu-latory characteristics associatedwith voice quality
Voice quality
Lower lips
Jaw
Tongue tip
F0
American English (low AQ)Japanese (sharp spectral slope)
lowered
raised
raised
raised
raised
We  also  examined  correlations  between  voice  quality  and  acoustic/articulatorymeasures as shown in figure 8. Table 4 summarizes the acoustic and articulatory char-acteristics associated with voice quality.
For the Japanese speaker, there was a significant correlation (p ⬍ 0.01) betweenvoice quality and F0 (r ⫽ ⫺0.64), with spectral slope becoming steeper as F0 becomeshigher. There was also a significant correlation between spectral slope and lower LL-y(r ⫽ 0.61,  p ⬍ 0.05),  and  Jy  (r ⫽ ⫺0.63,  p ⬍ 0.01)  with  spectral  slope  becomingsteeper as the jaw and lower lip were more raised.
For the American English speaker, F0 appears to be negatively correlated with AQin the high F0 range (300 z and above), but not in the modal F0 range (around 200 Hz)which does not show any correlation. There could be a connection between cricothyroidactivity and AQ which could account for this distribution pattern. At normal F0, presum-ably when the cricothyroid is less active, this speaker tends to have a breathy voice withAQ greater than 1.0, but at high F0 when the vocal folds are more tense due to increasedcricothyroid activity, we see lower AQ values. The relationship between speaking ranges(high, modal, low) and AQ is an interesting topic for further investigation.
In  addition,  there  was  a  significant  correlation  between AQ  and T1y  (r ⫽ 0.64,p ⬍ 0.01). In the mid range of tongue tip height, the scatter plot shows no relationshipwith AQ; only at the extremes of tongue tip height do we see a correlation such that lowAQ is associated with low tongue tip and high AQ with high tongue tip. This needs tobe investigated further.
The results suggest a relation for American English between low AQ and raised F0,as well as lowered tongue tip, and for Japanese, between steep spectral slope and raisedF0, raised jaw, lowered lip. Perhaps the maneuvers during imitated sadness of loweringthe tongue tip (in the case of American English) or raising of the lips (in the case ofJapanese) pull the larynx forward, change the vocal fold dynamics, and bring aboutirregular phonation, and in this way, contribute to either steepness of spectral slope orcreakiness  of  voice,  and  consequently,  to  the  listener’s  perception  of  sadness.  Thishypothesis needs to be explored further.
Conclusion
====================

This study reports on a novel, replicable method to record acoustic and articula-tory data for spontaneous emotional speech. The study has many shortcomings. Thefindings are based on two speakers and two utterance types spoken on different vowelnuclei. It tries to answer too many questions: How does spontaneous sadness comparewith imitated sadness compare with read speech (compare with imitated intonation)compare with speech that is highly rated by listeners as sad? How do spontaneous sad-ness, imitated sadness, perceived sadness compare across two different languages (i.e.,English and Japanese)?
Sad Speech
Phonetica 2006;63:1–25
19
However,  since  the  nature  of  the  study  is  such  that  it  is  not  possible  to  have  alaboratory-type highly controlled experimental design, an initial exploratory approachof this type was taken in order to better know how to proceed in the future to investigatethe topic of spontaneous emotion in speech.
The results of this study suggest that sad speech for both American English andJapanese is characterized by high F0, and a tendency for changed F1, as well as voicequality, e.g., change in glottal opening cycle characteristics/spectral tilt.
However, we see differences in terms of articulation. For the American speaker,the upper lip was retracted, the lower lip was retracted and raised while the jaw wasretracted and lowered, the tongue tip fronted and lowered, the tongue blade fronted,and the tongue dorsum fronted and raised for spontaneous sad speech; for imitated sadspeech, the upper and lower lips were protruded and lowered, the jaw was protrudedand raised, the tongue tip, backed and raised, the tongue blade backed and the tonguedorsum, backed and lowered. For the Japanese speaker, both upper and lower lips wereretracted and lowered, the jaw was protruded and lowered, and the tongue tip frontedfor spontaneous sad speech; for imitated sad speech both upper and lower lips wereprotruded and raised, the jaw was retracted and raised, and the tongue tip, backed.
The results suggest that articulation of strong emotions by a speaker, such as cry-ing while at the same time forcing oneself to speak, is different from acted/imitatedemotion.  Further  exploration  is  needed  into  possible  physiological  connectionsbetween emotion and speech in terms of production and perception, as well as into bio-logical underpinnings of sad speech.
Imitated  sad speech  showed  a  tendency  (although  no  significant  differences  werefound) for even more changed voice quality characteristics, i.e., more change in glottalopening characteristics or steeper spectral tilt, which perhaps contributed to listeners ratingimitated sadness as more sad than spontaneous sadness. One reason for this may be thatfor imitated sadness, the speaker is trying to ‘convey’ sadness, but real sadness is the resultof the speaker’s internal state, something that the speaker does not control that happens inspite of the speaker’s intentions. That spontaneous sadness has a distinctly different patternof articulation involving the jaw and lips (and tongue for American English) may be not forthe purpose of conveying sadness, but be the by-product of experiencing sadness.
The fact that both the Japanese and the American English speaker in imitating sad-ness, pouted lips for imitated sadness, i.e., the upper and lower lips were protruded, fur-thermore suggests there may be ‘universal’ postures for imitating sadness that are notnecessarily the same as those for producing spontaneous sadness. In acted emotion, thespeaker is volitionally changing the acoustic signal to impart to the listener a mental oremotional state (paralanguage) while in spontaneous emotion the speaker is working atmaintaining the acoustic signal to convey the intended message even through emotionalinterruptions  (nonlanguage).  From  the  results  of  this  study,  we  see  it  is  important  toclearly identify what a researcher of emotion intends to study and to choose the righttesting paradigm. Using actors in emotion studies will give results on paralanguage andnot results on emotion as such. Therefore, to study emotional characteristics of speech(articulatory phonetics, etc.) researchers need to move away from using actors.
An interesting result with imitated emotion is that the speakers seem to have beensensitive to imitating certain parts of their spontaneous sad speech, but not to others. Forinstance, the American English speaker (a phonetician) imitated F0, F1 and voice qualityof  the  sad speech  rather  successfully,  but  not  duration.  It  was  as  if  duration  was‘recalled’ from a set of stereotypes of what constitutes sad speech. A similar situation various  studies  about  the  multicomplex  richness  of  expressive  speech,  see  e.g.,Schroeder [2004], Gobl and Ní Chasaide [2003] and Douglas-Cowie et al. [2003].
20
Phonetica 2006;63:1–25
Erickson/Yoshida/Menezes/Fujino/Mochida/Shibuya
With regard to imitated intonation (in which the American English speaker imitatedthe intonational and phrasing patterns, but not F0 range), imitated intonation speech is(a) different from spontaneous sad speech/imitated sad speech in terms of acoustics andarticulation, (b) not rated highly by listeners as sad, and (c) shows low correlation of itsacoustic  characteristics  with  speech  that  was  highly  rated  by  listeners  as  sad. Theacoustic characteristic that correlated highly with listeners ratings of sadness were highF0 and low AQ (both characteristics of spontaneous sad or imitated sad speech, but notimitated intonation speech). These results suggest that intonation and phrasing patternsmay not be sufficient to convey emotional information to listeners, a finding which isreminiscent of that reported by Menezes [2004] who showed that phrasing patterns inthemselves were not salient characteristics of irritated utterances (vs. non-irritated utter-ances). More research needs to be done in this area in order to better understand the rela-tionship between intonation, phrasing, F0 range, and voice quality.
With regard to the rating of sadness by listeners, we see a difference between the waya speaker articulates spontaneous sad speech and those articulatory characteristics which‘convey’ sadness to a listener. For both American English and Japanese, speech that wasgiven a high rating for sadness by listeners, involved lip-pouting with raised and/or pro-truded jaw/lower lip. However, in actual articulation of spontaneous sad speech, we see theopposite: lowered and retracted jaw and retracted lower lip for American English and low-ered jaw and lowered retracted lower lip for Japanese. The pattern of articulation for well-perceived  sad speech  seems  to  be  similar  to  that  for  imitated  sad speech,  rather  thanspontaneous sad speech. This is a very interesting finding and needs to be explored further.With regard to whether there are common characteristics in the acoustics and artic-ulation  of  emotional  speech  in  such  different  languages  as  American  English  andJapanese,  the  tentative  results  from  this  study  (based  on  one  speaker  each)  suggestthere are common characteristics. In terms of acoustics, both speakers raised F0 andtended to change F1 and voice quality. In terms of articulation, both speakers retractedtheir upper and lower lips and lowered their jaw.
With regard to whether there are similarities/differences between perception of sadspeech as a function of the language: The tentative results of this study suggest that listen-ers use similar cues to rate sadness, i.e., high F0, changed F1, and changed voice quality.
To briefly summarize our findings, we can say that (1) the acoustic and articula-tory characteristics of spontaneous sad speech differ from that of read speech, or imi-tated intonation speech, (2) spontaneous sad speech and imitated sad speech seem tohave similar acoustic characteristics (high F0, changed F1 as well as voice quality forboth the American English and Japanese speaker) but different articulation, (3) thereare similarities in the way they imitate sadness, i.e., lip protrusion, and (4) the articulatorycharacteristics of imitated sad speech tend to show better correlation with ratings ofsadness by listeners than do those of spontaneous sad speech.
The method and tentative results of this study are reported here in order to serveas guidelines  for  investigating  various  acoustic  and  articulatory  characteristics  of various  studies  about  the  multicomplex  richness  of  expressive  speech,  see  e.g.,Schroeder [2004], Gobl and Ní Chasaide [2003] and Douglas-Cowie et al. [2003].
Sad Speech
Phonetica 2006;63:1–25
21
Acknowledgements
==========================

We  thank  NTT  Communication  Science  Labs  for  allowing  us  to  use  the  EMA  facilities,  andParham Mokhtari for his help in analyzing the acoustic American English data, specifically voice qual-ity. I also wish to thank two anonymous reviewers for their extremely helpful comments, and especially,Klaus Kohler for his encouragement to revise the manuscript in order that this proof of method researchcould  be  published.  This  study  is  supported  by  a  Grant-in-Aid  for  Scientific  Research,  JapaneseMinistry of Education, Culture, Sports, Science and Technology (2002–5): 14510636 to the first author.
Appendix
==================

Table 1A. Averaged acousticvalues of E, IE, II, and R speech(American English)
Cat.
Dur; ms
F0, Hz
F1, Hz
F2, Hz
AQ
Percept.
EIEIIR
250 370 240 300
327 335 187 199
355 342 338 335
2381 2077 2579 2623
0.97 0.94 1.42 1.21
4.64.21.51.3
Table 2A. Averaged acousticvalues of E, IE, II, and R speech(Japanese)
Cat. Dur; ms
F0, Hz
F1, Hz
F2, Hz
S Slope
Percept.
EIER
210 175 157
190 184 150
796 785 861
1496  ⫺0.019 1648  ⫺0.023 1682  ⫺0.013
3.23.72.4
Table  3A. ANOVA  results  ofacoustic measures with utteranceconditions
Speaker
American English
Japanese
Dependentvariable
durationF0F1AQdurationF0F1spectral slope
d.f.
F
p
3, 123, 123, 123, 122, 112, 112, 112, 11
6.28470.6010.33813.675.438.0171.433.365
p ⬍ 0.01p ⬍ 0.000p ⫽ 0.798p ⬍ 0.000p ⬍ 0.05p ⬍ 0.000p ⫽ 0.280p ⫽ 0.072
22
Phonetica 2006;63:1–25
Erickson/Yoshida/Menezes/Fujino/Mochida/Shibuya
Table 4A. Averaged articulatory values of E, IE, II, and R speech (American English)
Cat.
Jx
Jy
ULx ULy
LLx
LLy
T1x
T1y
T2x
T2y
T3x
T3y
EIEIIR
7.247.037.187.38
12.0712.2112.1712.15
6.64 6.03 6.01 6.12
14.66 14.38 14.50 14.41
6.45 5.84 5.93 6.15
13.06 12.70 12.58 12.52
7.85 8.39 8.48 8.53
13.73 13.93 14.11 13.99
8.87 9.46 9.52 9.44
15.09 14.82 14.87 14.99
9.9511.7511.7711.81
15.4714.1914.2114.36
Table 5A. Averaged articulatory values of E, IE, II, and R speech (Japanese)
Cat.
Jx
Jy
ULx
ULy
LLx
LLy
T1x
T1y
T2x
T2y
EIER
7.78 8.18 8.25
12.63 12.81 12.64
5.92 5.37 5.44
15.12 15.42 15.31
6.51 6.19 6.39
13.13 13.42 13.08
8.81 9.34 9.24
14.12 14.14 14.05
9.96 10.04 10.00
14.6214.7914.89
Table  6A. ANOVA  results  ofarticulatory measures with utter-ance conditions
Speaker
American English
Japanese
Dependent variable
d.f.
F
p
ULxULyLLxLLyJxJyT1xT1yT2xT2yT3xT3yULxULyLLxLLyJxJyT1xT1yT2xT2y
3, 123, 123, 123, 123, 123, 123, 123, 123, 123, 123, 123, 122, 112, 112, 112, 112, 112, 112, 112, 112, 112, 11
63.0872.25726.65214.13721.9224.67643.7764.60817.1893.724107.417393.784725.195200.8359.8920.83562.7814.1389.8620.2370.1010.871
p ⬍ 0.000p ⫽ 0.134p ⬍ 0.000p ⬍ 0.000p ⬍ 0.000p ⬍ 0.05p ⬍ 0.000p ⬍ 0.05p ⬍ 0.000p ⫽ 0.042p ⬍ 0.000p ⬍ 0.000p ⬍ 0.000p ⬍ 0.000p ⬍ 0.000p ⬍ 0.000p ⬍ 0.000p ⬍ 0.01p ⬍ 0.01p ⫽ 0.793p ⫽ 0.904p ⫽ 0.446
Sad Speech
Phonetica 2006;63:1–25
23
References
====================

Alku,  P.;  Backstrom,  T.;  Vilkman,  E.:  Normalized  amplitude  quotient  for  parameterization  of  the  glottal  flow.
J. acoust. Soc. Am. 112: 701–710 (2002).
Alku,  P.; Vilkman,  E.: Amplitude  domain  quotient  for  characterization  of  the  glottal  volume  velocity  waveform
estimated by inverse filtering. Speech Com. 18: 131–138 (1996).
Broad, D.J.; Clermont, F.: Formant estimation by linear transformation of the LPC cepstrum. J. acoust. Soc. Am.
86: 2013–2017 (1986).
Brown, W.A.; Sirota, A.D.; Niaura, R.; Engebretson, T.O.: Endocrine correlates of sadness and elation. Psychosom.
Med. 55: 458–467 (1993).
Bühler, K.: Sprachtheorie; 2nd ed. (Fischer, Stuttgart 1965).Campbell, N.; Erickson, D.: What do people hear? A study of the perception of non-verbal affective information in
conversational speech. J. phonet. Soc. Japan 8: 9–28 (2004).
Childers, D.G.; Lee, C.K.: Vocal quality factors: analysis, synthesis, perception. J. acoust. Soc. Am. 90: 2394–2410
(1991).
Cowie, R.; Douglas-Cowie, E.; Schroeder, M. (eds.): Proceedings of the ISCA Workshop on Speech and Emotion:
A Conceptual Framework for Research, Belfost (2000).
Darwin, C.: The expression of the emotions in man and animals; 3rd ed. (Oxford University Press, Oxford 1998).Douglas-Cowie, E.; Campbell, N.; Cowie, R.; Roach, P.: Emotional speech: towards a new generation of databases.
Speech Commun. 40: spec. issue Speech and Emotion, pp. 33–60 (2003).
Eldred, S.H.; Price, D.B.: A linguistic evaluation of feeling states in psychotherapy. Psychiatry 21: 115–121 (1958).Erickson, D.: Articulation of extreme formant patterns for emphasized vowels. Phonetica 59: 134–149 ( 2002).Erickson, D.: Expressive speech: production, perception and application to speech synthesis. J. acoust. Soc. Japan
26: 4 (2005).
Erickson,  D.; Abramson, A.;  Maekawa,  K.;  Kaburagi,  T.: Articulatory  characteristics  of  emotional  utterances  in
spoken English. Proc. Int. Conf. Spoken Lang. Processing, vol 2, pp. 365–368 (2000).
Erickson, D.; Bauer, H.; Fujimura, O.: Non-F0 correlates of prosody in free conversation. J. acoust. Soc. Am. 88:
S128 (1990).
Erickson, D.; Fujimura, O.; Pardo, B.: Articulatory correlates of prosodic control: emotion versus emphasis. Lang.
Speech 41: spec. issue Prosody and Conversation, pp. 399–417 (1998).
Erickson, D.; Fujino, A.; Mochida, T.; Menezes, C.; Yoshida, K.; Shibuya, Y: Articulation of sad speech: comparison
of American English and Japanese. Acoust. Soc. Japan, Fall Meeting, 2004a.
Erickson, D.; Menezes, C.; Fujino, A.: Sad speech: some acoustic and articulatory characteristics. Proc. 6th Int.
Semin. Speech Prod., Sydney, Dec. 2003a.
Erickson, D.; Menezes, C.; Fujino, A.: Some articulatory measurements of real sadness (ThA3101p.3) Proc. Int.
Conf. Spoken Lang. Processing, Jeju, Oct 2004b.
Erickson, D.; Mokhtari, P.; Menezes, C.; Fujino, A.: Voice quality and other acoustic changes in sad speech (grief).
IEICE Tech. Rep. SP2003 June. ATR: 43–48 (2003b).
Erickson,  D.; Yoshida,  K.;  Mochida, T.;  Shibuya, Y.: Acoustic  and  articulatory  analysis  of  sad Japanese  speech.
Phonet. Soc. Japan, Fall Meeting, 2004c.
Fant,  G.;  Kruckenberg,  A.;  Liljencrants,  J.;  Bavergard,  M.:  Voice  source  parameters  in  continuous  speech.
Transformation of LF parameters. Proc. Int. Conf. Spoken Lang. Processing, 1994, pp. 1451–1454.
Fujisaki,  H.:  Information,  prosody,  and  modelling  –  with  emphasis  on  tonal  features  of  speech.  Proc.  Speech
Prosody 2004, Nara 2004, pp. 1–10.
Gerratt, B.; Kreiman, J.: Toward a taxonomy of nonmodal phonation, J. Phonet. 29: 365–381 (2001).Gobl,  C.;  Ní  Chasaide,  A.:  The  role  of  voice  quality  in  communicating  emotion,  mood,  and  attitude.  Speech
Commun. 40: 189–212 (2003).
Hanson,  H.M.;  Stevens,  K.N.;  Kuo,  H.-K.J.;  Chen,  M.Y.;  Slifka,  J.:  Towards  a  model  of  phonation.  J.  Phonet.
29: 451–480 (2001).
Iida, A.: A study on corpus-based speech synthesis with emotion; thesis, Keio (2000).Ishii, C.T.: A new acoustic measure for aspiration noise detection. (WeA501p.10) Proc. Int. Conf. Spoken Lang.
Processing, Jeju, Oct. 2004.
Jackson, M.; Ladefoged, P.; Huffman, M.K.; Antoñanzas-Barroso, N.: Measures of spectral tilt. UCLA Working
Papers Phonet 61: 72–78 (1985).
Johnson, K.: Acoustic and auditory phonetics (Blackwell Publishing, Malden 2003).Kaburagi, T.; Honda, M.: Calibration methods of voltage-to-distance function for an electromagnetic articulometer
(EMA) system J. acoust. Soc. Am. 111: 1414–1421 (1997).
Klatt, D.; Klatt, L.: Analysis, synthesis and perception of voice quality variati`ons among female and male talkers.
J. acoust. Soc. Am. 87: 820–857 (1990).
Maekawa, K.; Kagomiya, T.; Honda, M.; Kaburagi, T.; Okadome, T.: Production of paralinguistic information: from
on articulatory point of view. Acoust. Soc. Japan: 257–258 (1999).
Mazo, M.; Erickson, D.; Harvey, T.: Emotion and expression, temporal date on voice quality in Russian lament. 8th
Vocal Fold Physiology Conference, Kurume 1995, pp. 173–187.
Menezes, C.: Rhythmic pattern of American English: An articulatory and acoustic study; PhD Columbus (2003).
24
Phonetica 2006;63:1–25
Erickson/Yoshida/Menezes/Fujino/Mochida/Shibuya
Menezes, C.: Changes in phrasing in semi-spontaneous emotional speech: articulatory evidences. J. Phonet. Soc.
Japan 8: 45–59 (2004).
Mitchell, C.J.; Menezes, C.; Williams, J.D.; Pardo, B.; Erickson, D.; Fujimura, O.: Changes in syllable and bound-
ary strengths due to irritation. ISCA Workshop on Speech and Emotion, Belfast 2000.
Mokhtari,  P.;  Campbell,  N.:  Perceptual  validation  of  a  voice  quality  parameter  AQ  automatically  measured  in
acoustic islands of reliability. Acoust. Soc. Japan: 401–402 (2002).
Mokhtari, P.; Campbell, N: Automatic measurement of pressed/breathy phonation at acoustic centres of reliability incontinuous  speech.  Proc.  IEICE  Trans.  Information  Syst  E-86-D:  Spec.  Issue  on  Speech  Information,pp. 574–582 (2003).
Mokhtari, P.; Iida, A.; Campbell, N.: Some articulatory correlates of emotion variability in speech: a preliminary
study on spoken Japanese vowels. Proc. Int. Conf. Speech Processing, Taejon 2001, pp. 431–436.
Ní  Chasaide,  A.;  Gobl,  C.:  Voice  source  variation;  in  Hardcastle,  Laver,  The  handbook  of  phonetic  sciences
(Blackwell, Oxford 1997), pp. 427–461.
Redi,  L.;  Shattuck-Hufnagel,  S.: Variation  in  the  realization  of  glottalization  in  normal  speakers.  J.  Phonet.  29:
407–429 (2001).
Sadanobu, T.: A natural history of Japanese pressed voice. J. Phonet. Soc. Japan 8: 29–44 (2004).Scherer, K.R.: Vocal communication of emotion: A review of research paradigms. Speech Commun. 40: 227–256
(2003).
Scherer, K.R.; Nonlinguistic vocal indicators of emotion and psychopathology; in Izard, Emotions in personality
and psychopathology, pp. 493–529 (Plenum Press, New York 1979).
Scherer, K.R.; Banse, R.; Wallbott, H.G.; Goldbeck, T.: Vocal cues in emotion encoding and decoding. Motivation
Emotion 15: 123–148 (1991).
Schroeder, M.: Speech and emotion research: an overview of research frameworks and a dimensional approach to
emotional speech synthesis; thesis 7, Saarbrücken (2004).
Schroeder, M.; Aubergé, V.; Cathiard, M.A.: Can we hear smile? Proc. 5th Int. Conf. Spoken Lang. Processing,
Sydney 1998.
Sad Speech
Phonetica 2006;63:1–25
25
All in-text references underlined in blue are linked to publications on ResearchGate, letting you access and read them immediately.
