# -*- coding: utf-8 -*
from mendeley_manager import Mendeley_Manager
from pdf_manager import PDF_Manager
m = Mendeley_Manager()
a = m.get_article('Perception and production boundaries between single and geminate stops in Japanese.')
p = PDF_Manager(a.filepath)
